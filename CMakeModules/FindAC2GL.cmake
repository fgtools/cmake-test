#
# Try to find AC2GL library and include path.
# Once done this will define
#
# AC2GL_FOUND
# AC2GL_INCLUDE_PATH
# AC2GL_LIBRARY
#

IF (WIN32)
 FIND_PATH( AC2GL_INCLUDE_PATH ac3d.hxx
       ${AC2GL_ROOT_DIR}/include
     DOC "The directory where ac3d.hxx resides")
 IF (NV_SYSTEM_PROCESSOR STREQUAL "AMD64")
   FIND_LIBRARY( AC2GL_LIBRARY
     NAMES ac2gl64
     PATHS ${AC2GL_ROOT_DIR}/lib64
     DOC "The AC2GL library (64-bit)")
 ELSE(NV_SYSTEM_PROCESSOR STREQUAL "AMD64")
    FIND_LIBRARY( AC2GL_LIB_REL
        NAMES ac2gl
        PATHS ${AC2GL_ROOT_DIR}/lib
        DOC "The AC2GL Release library")
    FIND_LIBRARY( AC2GL_LIB_DBG
        NAMES ac2gld
        PATHS ${AC2GL_ROOT_DIR}/lib
        DOC "The AC2GL Debug library")
    if (AC2GL_LIB_REL)
        set(AC2GL_LIBRARY "optimized;${AC2GL_LIB_REL}")
        if (AC2GL_LIB_DBG)
            list(APPEND AC2GL_LIBRARY "debug;${AC2GL_LIB_DBG}")
        else ()
            list(APPEND AC2GL_LIBRARY "debug;${AC2GL_LIB_REL}")
        endif ()
    endif ()
 ENDIF(NV_SYSTEM_PROCESSOR STREQUAL "AMD64")
ELSE (WIN32)
 FIND_PATH( AC2GL_INCLUDE_PATH ac3d.hxx
   /usr/include
   /usr/local/include
   /sw/include
   /opt/local/include
   ${AC2GL_ROOT_DIR}/include
   DOC "The directory where ac3d.hxx resides")
 FIND_LIBRARY( AC2GL_LIBRARY
   NAMES ac2gl
   PATHS
    /usr/lib
    /usr/lib64
    /usr/local/lib64
    /usr/local/lib
    /sw/lib
    /opt/local/lib
    ${AC2GL_ROOT_DIR}/lib
   DOC "The AC2GL library")
ENDIF (WIN32)

SET(AC2GL_FOUND "NO")
IF (AC2GL_INCLUDE_PATH AND AC2GL_LIBRARY)
 SET(AC2GL_LIBRARIES ${AC2GL_LIBRARY})
 SET(AC2GL_FOUND "YES")
ENDIF (AC2GL_INCLUDE_PATH AND AC2GL_LIBRARY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(AC2GL DEFAULT_MSG AC2GL_LIBRARY AC2GL_INCLUDE_PATH)

# eof
