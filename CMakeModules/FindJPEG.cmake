# - Find JPEG - modified 20130704
# Find the native JPEG includes and library
# This module defines
#  JPEG_INCLUDE_DIR, where to find jpeglib.h, etc.
#  JPEG_LIBRARIES, the libraries needed to use JPEG.
#  JPEG_FOUND, If false, do not try to use JPEG.

#=============================================================================
# Copyright 2001-2009 Kitware, Inc.
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)

find_path(JPEG_INCLUDE_DIR jpeglib.h)

if (WIN32)
 find_library(JPEG_LIBRARY_REL NAMES jpeg )
 find_library(JPEG_LIBRARY_DBG NAMES jpegd )
 if (JPEG_LIBRARY_REL)
  if (JPEG_LIBRARY_DBG)
   set(JPEG_LIBRARY "optimized;${JPEG_LIBRARY_REL};debug;${JPEG_LIBRARY_DBG}")
  else ()
   set(JPEG_LIBRARY ${JPEG_LIBRARY_REL})
  endif ()
 endif ()
else ()
set(JPEG_NAMES ${JPEG_NAMES} jpeg)
find_library(JPEG_LIBRARY NAMES ${JPEG_NAMES} )
endif ()

# handle the QUIETLY and REQUIRED arguments and set JPEG_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(JPEG DEFAULT_MSG JPEG_LIBRARY JPEG_INCLUDE_DIR)

if(JPEG_FOUND)
  set(JPEG_LIBRARIES ${JPEG_LIBRARY})
endif()

mark_as_advanced(JPEG_LIBRARY JPEG_INCLUDE_DIR )

# eof
