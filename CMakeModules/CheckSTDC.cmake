# 20140722 - 20140527 - A simple check for ANSI C header files
message(STATUS "Checking whether system has ANSI C header files")
include(CheckPrototypeExists)
include(CheckIncludeFiles)

# this is NOT an exhaustive list! for more
# see : http://en.wikipedia.org/wiki/C_standard_library
# see : http://stackoverflow.com/questions/2027991/list-of-standard-header-files-in-c-and-c
# and reallly the list should change for windows - this LIMIT list also found in Windows
set(StandardHeadersExist)
set(_HDR_LIST stdint.h stddef.h stdlib.h string.h float.h)
check_include_files("${_HDR_LIST}" StandardHeadersExist)
### check_include_files("stdint.h;stddef.h;stdlib.h;string.h;float.h" StandardHeadersExist)
if(StandardHeadersExist)
    set(memchrExists)
    set(freeExists)
	check_prototype_exists(memchr string.h memchrExists)
    check_prototype_exists(free stdlib.h freeExists)
	if(memchrExists AND freeExists)
        message(STATUS "ANSI C header files - found ${_HDR_LIST}")
        set(STDC_HEADERS 1 CACHE INTERNAL "System has ANSI C header files")
        set(HAVE_STDINT_H 1 CACHE INTERNAL "System has stdint.h header file")
        set(HAVE_STDDEF_H 1 CACHE INTERNAL "System has stddef.h header file")
        set(HAVE_STDLIB_H 1 CACHE INTERNAL "System has stdlib.h header file")
        set(HAVE_STRING_H 1 CACHE INTERNAL "System has string.h header file")
        set(HAVE_FLOAT_H 1 CACHE INTERNAL "System has float.h header file")
	endif()
endif()

if(NOT STDC_HEADERS)
	message(STATUS "1 or more ANSI C header files NOT FOUND")
	set(STDC_HEADERS 0 CACHE INTERNAL "System does NOT have all ANSI C header files")
endif()

# eof
