#!/bin/sh
BN=`basename $0`
TMPLOG="bldlog-p.txt"

if [ -f "$TMPLOG" ]; then
    rm -f "$TMPLOG"
fi

echo "$BN: Running: 'make package' out to $TMPLOG"
echo "$BN: Running: 'make package'" > $TMPLOG
make package >> $TMPLOG 2>&1
if [ ! "$?" = "0" ]; then
    echo "$BN: 'make package' FAILED!"
    exit 1
fi
echo "$BN: make package done... no error... see $TMPLOG"

# eof

