@setlocal
@set TMP3RD=D:\Projects\3rdParty.x64
@set TMPGEN=Visual Studio 16 2019
@set TMPSRC=..
@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOCM

@set TMPOPTS=-G "%TMPGEN%" -A x64

@REM #######################################
@REM *** ADJUST TO SUIT YOUR ENVIRONMENT ***
@set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH:PATH=%TMP3RD%
@REM set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX:PATH=
@REM #######################################
@set TMPVER=1

@call chkmsvc Example

@set TMPLOG=bldlog-1.txt
@REM Uncomment if multiple build logs required
@REM set TMPLOG=bldlog-%TMPVER%.txt
@REM :RPT
@REM if NOT EXIST %TMPLOG% goto GOTLOG
@REM set /A TMPVER+=1
@REM set TMPLOG=bldlog-%TMPVER%.txt
@REM goto RPT
@REM :GOTLOG

@echo Building source: [%TMPSRC%] %DATE% %TIME% output to %TMPLOG%
@echo Building source: [%TMPSRC%] %DATE% %TIME% > %TMPLOG%

cmake -S %TMPSRC% %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

@REM if these are also required
@REM cmake --build . --config RelWithDebInfo >> %TMPLOG% 2>&1
@REM if ERRORLEVEL 1 goto ERR4

@REM cmake --build . --config MinSizeRel >> %TMPLOG% 2>&1
@REM if ERRORLEVEL 1 goto ERR5

cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3
:DONEREL

@echo Appear successful...  >> %TMPLOG% 2>&1
@echo Appear successful...
@goto END

:NOCM
@echo ERROR: Can NOT locate %TMPSRC%\CMakeLists.txt! WHERE IS IT! FIX ME!!!
@goto ISERR

:ERR1
@echo cmake config gen ERROR!
@goto ISERR

:ERR2
@echo cmake build Debug ERROR!
@goto ISERR

:ERR3
@fa4 "mt.exe : general error c101008d:" %TMPLOG% >nul
@if ERRORLEVEL 1 goto ERR33
:ERR34
@echo ERROR: Cmake build Release FAILED!
@goto ISERR
:ERR33
@echo Try again due to this STUPID STUPID STUPID error
@echo Try again due to this STUPID STUPID STUPID error >>%TMPLOG%
cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR34
@goto DONEREL

:ERR4
@echo cmake build RelWithDebInfo ERROR!
@goto ISERR

:ERR5
@echo cmake build MinSizeRel ERROR!
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
