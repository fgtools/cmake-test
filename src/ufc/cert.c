
/*
 * This crypt(3) validation program shipped with UFC-crypt
 * is derived from one distributed with Phil Karns PD DES package.
 *
 * @(#)cert.c	1.8 11 Aug 1996
 */

#include <stdio.h>
#include <stdlib.h> // for exit in WIN32
#include "crypt.h"

#define encrypt ufc_encrypt
#define setkey ufc_setkey

static int totfails = 0;
static int tottests = 0;

#if __STDC__ - 0
int main (int argc, char *argv[]);
void get8 (char *cp);
void put8 (char *cp);
void good_bye (void) __attribute__ ((noreturn));
#else
void get8(), put8();
#endif

void good_bye ()
{
    if (tottests)
        printf("Done %d tests: ", tottests);
  if(totfails == 0) {
    printf("Passed all DES validation suite\n");
    exit(0);
  } else {
    printf("%d failures during DES validation suite!!!\n", totfails);
  }
  exit(1);
}

#ifdef _MSC_VER

static FILE *fp;
void get8(char *cp)
{
	int i,j,t;

	for(i=0;i<8;i++){
		fscanf(fp,"%2x",&t);
		if(feof(fp))
		  good_bye();
		for(j=0; j<8 ; j++) {
		  *cp++ = (t & (0x01 << (7-j))) != 0;
		}
	}
}
void put8(char *cp)
{
	int i,j,t;

	for(i=0;i<8;i++){
	  t = 0;
	  for(j = 0; j<8; j++)
	    t = (t<<1) | *cp++;
	  printf("%02x", t);
	}
}

int main(int argc, char **argv)
{
	char key[64],plain[64],cipher[64],answer[64];
	int i;
	int test;
	int fail;
    if (argc < 2) {
        fprintf(stderr,"Usage: %s input-file\n", argv[0]);
        return 1;
    }
    fp = fopen(argv[1],"r");
    if (!fp) {
        fprintf(stderr,"Error: Unable to open file %s\n", argv[1]);
        return 1;
    }
	for(test=0;!feof(fp);test++){

		get8(key);
		printf(" K: "); put8(key);
		setkey(key);

		get8(plain);
		printf(" P: "); put8(plain);

		get8(answer);
		printf(" C: "); put8(answer);

        tottests++;

		for(i=0;i<64;i++)
			cipher[i] = plain[i];
		encrypt(cipher, 0);

		for(i=0;i<64;i++) {
			if(cipher[i] != answer[i])
				break;
        }
		fail = 0;
		if(i != 64){
			printf(" Encrypt FAIL");
			fail++; totfails++;
		}

		encrypt(cipher, 1);

		for(i=0;i<64;i++) {
			if(cipher[i] != plain[i])
				break;
        }
		if(i != 64){
			printf(" Decrypt FAIL");
			fail++; totfails++;
		}

		if(fail == 0)
			printf(" OK");
		printf("\n");
	}
	good_bye(); // usually not reached - exit in get8() EOF condition
}

#else // !_MSC_VER

int
main(argc, argv)
     int argc;
     char *argv[];
{
	char key[64],plain[64],cipher[64],answer[64];
	int i;
	int test;
	int fail;

	for(test=0;!feof(stdin);test++){

		get8(key);
		printf(" K: "); put8(key);
		setkey(key);

		get8(plain);
		printf(" P: "); put8(plain);

		get8(answer);
		printf(" C: "); put8(answer);

		for(i=0;i<64;i++)
			cipher[i] = plain[i];
		encrypt(cipher, 0);

		for(i=0;i<64;i++)
			if(cipher[i] != answer[i])
				break;
		fail = 0;
		if(i != 64){
			printf(" Encrypt FAIL");
			fail++; totfails++;
		}

		encrypt(cipher, 1);

		for(i=0;i<64;i++)
			if(cipher[i] != plain[i])
				break;
		if(i != 64){
			printf(" Decrypt FAIL");
			fail++; totfails++;
		}

		if(fail == 0)
			printf(" OK");
		printf("\n");
	}
	good_bye();
}
void
get8(cp)
char *cp;
{
	int i,j,t;

	for(i=0;i<8;i++){
		scanf("%2x",&t);
		if(feof(stdin))
		  good_bye();
		for(j=0; j<8 ; j++) {
		  *cp++ = (t & (0x01 << (7-j))) != 0;
		}
	}
}
void
put8(cp)
char *cp;
{
	int i,j,t;

	for(i=0;i<8;i++){
	  t = 0;
	  for(j = 0; j<8; j++)
	    t = (t<<1) | *cp++;
	  printf("%02x", t);
	}
}

#endif // _MSC_VER y/n
/* eof */
