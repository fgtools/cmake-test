README.txt - 20130718

This source was obtained from http://ftp.nluug.nl/crypto/gnu/
DOWN: 28,791 glibc-crypt-2.0.6.tar.gz
TAR: This tar had the source in different folder, but here they are all in one.

Test Files: (with main())

cert.c - reads a cert.input and verifies the output of encrypt
speeds.c - like it says, some type of speed test
ufc.c - again seems like a 'test' program

Library:

crypt-entry.c
crypt.c
crypt_util.c


crypt-private.h
crypt.h
patchlevel.h
ufc-crypt.h

