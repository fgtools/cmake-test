/*
 * UFC-crypt: ultra fast crypt(3) implementation
 *
 * Copyright (C) 1991, 1992, 1993, 1996 Free Software Foundation, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @(#)ufc.c	2.7 9/10/96
 *
 * Stub main program for debugging
 * and benchmarking.
 *
 */

#include <stdio.h>
#include <stdlib.h> // for exit(), atoi in unix
#include <string.h> // for strcmp() in unix
#ifdef WIN32
#include <Windows.h>
#else
#include <sys/time.h> // for gettimeofday()
#endif
// save including <crypt.h>
char *ufc_crypt(const char *, const char *);

///////////////////////////////////////////////////////////////////////////////
#define USE_PERF_COUNTER
///////////////////////////////////////////////////////////////////////////////
#if (defined(WIN32) && defined(USE_PERF_COUNTER))
// QueryPerformanceFrequency( &frequency ) ;
// QueryPerformanceCounter(&timer->start) ;
double get_seconds()
{
    static double dfreq;
    static int done_freq = 0;
    static int got_perf_cnt = 0;
    double d;
    if (!done_freq) {
        LARGE_INTEGER frequency;
        if (QueryPerformanceFrequency( &frequency )) {
            got_perf_cnt = 1;
            dfreq = (double)frequency.QuadPart;
        }
        done_freq = 1;
    }
    if (got_perf_cnt) {
        LARGE_INTEGER counter;
        QueryPerformanceCounter (&counter);
        d = (double)counter.QuadPart / dfreq;
    }  else {
        DWORD dwd = GetTickCount(); // milliseconds that have elapsed since the system was started
        d = (double)dwd / 1000.0;
    }
    return d;
}

#else // !WIN32
double get_seconds()
{
    struct timeval tv;
    gettimeofday(&tv,0);
    double t1 = (double)(tv.tv_sec+((double)tv.tv_usec/1000000.0));
    return t1;
}
#endif // WIN32 y/n
///////////////////////////////////////////////////////////////////////////////


int main(int argc, char **argv)
{
    char *s;
    unsigned long i,iterations;
    double bsecs;    
    if(argc != 2) {
      fprintf(stderr, "usage: ufc iterations\n");
      exit(1);
    }
    argv++;
    iterations = atoi(*argv);
    printf("ufc: running %ld iterations\n", iterations);
    
    bsecs = get_seconds();
    for(i=0; i<iterations; i++) {
      s = ufc_crypt("foob","ar");
      if(strcmp(s, "arlEKn0OzVJn.")) {
        printf("wrong result: [%s]!! expected [arlEKn0OzVJn.]\n", s);
        exit(1);
      }
    }
    bsecs = get_seconds() - bsecs;
    printf("ufc: Passed all %ld iterations in %f secs\n", i, bsecs);
    return 0;
}

/* eof */
