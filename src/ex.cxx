// ex.cxx

#include <sys/types.h>
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <stdio.h>
#include <time.h>
#include <string.h> // for strdup() in unix
#include <stdlib.h> // for exit() in unix
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <assert.h>
#ifndef _MSC_VER
#include <crypt.h>
#endif
#include "image.hxx"
#include "ex.hxx"
#include "sprtf.hxx"

#ifndef SPRTF
#define SPRTF sprtf
#endif

#ifdef _MSC_VER
#include "ufc/crypt.h"
#define snprintf _snprintf
#define strcasecmp stricmp
#define strncasecmp _strnicmp
#define MCRYPT  ufc_crypt_r
#define MCSTR ufc_crypt_data
#else
#define MCRYPT  crypt_r
#define MCSTR crypt_data
#endif

static struct MCSTR data = {0};

char *out_file = (char *)"tempout.fil";
char *int_pwd = (char *) "password";
//char *md5_pwd = (char *) "5f4dcc3b5aa765d61d8327deb882cf99";
//char *md5_pwd = (char *) "$1EIJ3J/E/E32";
//char *md5_pwd = (char *) "{crypt}$1EIJ3J/E/E32";
char *md5_pwd = (char *) "{crypt}$1Oyx5r9mdGZ2";
#define MAX_R_BUF 1024

static char _s_buffer[264];
// static char _s_rbuff[MAX_R_BUF]; // not used


#define DES_PREFIX "{crypt}"    /* to distinguish clear text from DES crypted */
#define MD5_PREFIX "$1$"

char *crypt2(const char *key, const char *salt)
{
//#ifdef _MSC_VER
//    char *cp = _s_rbuff; 
//#else
//    crypt_data *cp = (crypt_data *)_s_rbuff;
//#endif    
    struct MCSTR *cp = &data;
    memset(cp,0,sizeof(data));
    char *cp2 = MCRYPT ( key, salt, cp );
    return cp2;
}

int pass_matches( char* pass, char* tried_pass )
{
	//DEBUG d ( __FUNCTION__,__FILE__,__LINE__ );
	int des;
	if ( ( des = !strncasecmp ( pass, DES_PREFIX, sizeof ( DES_PREFIX )-1 ) ) )
	{
		pass += sizeof ( DES_PREFIX )-1;
	}
	if ( des || !strncmp ( pass, MD5_PREFIX, sizeof ( MD5_PREFIX )-1 ) )
	{
        SPRTF("Doing crypt [%s], with [%s]\n", tried_pass, pass );
        //char *temp = crypt ( tried_pass, pass );
        char *temp = crypt2 ( tried_pass, pass );
        SPRTF("crypt of [%s], salt [%s], yielded [%s]\n", tried_pass, pass, temp );
		tried_pass = temp;
	}
	return !strcmp ( pass, tried_pass );
}

int pass_matches2( char* pass, char* tried_pass )
{
    int res;
    SPRTF("Comparing [%s] with [%s]\n", pass, tried_pass);
    res = pass_matches( pass, tried_pass );
    return res;
}


void give_help(char *name)
{
	printf("%s version %s\n", name, EXAMPLE_VERSION);
    printf("Usage: [options] [out-file]\n");
    printf("Options:\n");
    printf(" --help (-h or -?) = This help and exit(2)\n");
    printf(" --pass word  (-p) = Compare pass with internal\n");
    exit(2);
}

int test2(char *pass)
{
    unsigned long seed[2];
    char salt[] = "$1$........";
    const char *const seedchars =
         "./0123456789ABCDEFGHIJKLMNOPQRST"
         "UVWXYZabcdefghijklmnopqrstuvwxyz";
    char *password;
    int i, cnt;
    //char *ps = salt;

    /* Generate a (not very) random seed.
       You should do it better than this... */
    seed[0] = time(NULL);
    seed[1] = getpid() ^ (seed[0] >> 14 & 0x30000);
     
    /* Turn it into printable characters from `seedchars'. */
    for (i = 0; i < 8; i++)
       salt[3+i] = seedchars[(seed[i/5] >> (i%5)*6) & 0x3f];
    
#if 0 // this seems wrong
    /* Read in the user's password and encrypt it. */
    //password = crypt(getpass("Password:"), salt);
    password = crypt(pass, ps);
    /* Print the results. */
    //puts(password);
    cnt = 0;
    SPRTF("crypt   pwd [%s], salt [%s], yielded [%s]\n", pass, salt, password);
    while (strcmp(ps,password)) {
        cnt++;
        ps = strdup(password);
        password = crypt(pass, ps);
        SPRTF("crypt %d pwd [%s], salt [%s], yielded [%s]\n", cnt, pass, ps, password);
        if (cnt >= 9)
            break;
    }
#endif // 0

    // ==============================================================
//#ifdef _MSC_VER
//    char *cb = _s_rbuff; 
//#else
//    crypt_data *cb = (crypt_data *)_s_rbuff;
//#endif    
    struct MCSTR *cb = &data;
    memset(cb,0,sizeof(data));
    password = MCRYPT(pass, salt, cb);
    char *nsalt = salt;
    SPRTF("crypt_r 0 pwd [%s], salt [%s], yielded [%s]\n", pass, nsalt, password);
    cnt = 0;
    while (strcmp(password,nsalt)) {
        cnt++;
        nsalt = strdup(password);
        memset(cb,0,sizeof(data));
        password = MCRYPT(pass, nsalt, cb);
        int ok = strcmp(nsalt,password) == 0;
        SPRTF("crypt_r %d pwd [%s], salt [%s], yielded [%s]. Access %s\n", cnt, pass, nsalt, password,
            (ok ? "granted" : "denied") );
        if (cnt >= 9)
            break;
    }
    return 0;
}

int test3(char *pass)
{
    /* Hashed form of "GNU libc manual". */
    const char *const pass_hash = "$1$/iSaq7rB$EoUw5jJPPvAPECNaaWzMK/";
     
    char *result;
    int ok;
//#ifdef _MSC_VER
//    char *cb = _s_rbuff; 
//#else
//    crypt_data *cb = (crypt_data *)_s_rbuff;
//#endif    
    struct MCSTR *cb = &data;
    memset(cb,0,sizeof(data));

    /* Read in the user's password and encrypt it,
       passing the expected password in as the salt. */
    //result = crypt(getpass("Password:"), pass);
    //result = crypt(pass, pass_hash);
    result = MCRYPT(pass, pass_hash, cb);
     
    /* Test the result. */
    ok = strcmp (result, pass_hash) == 0;
    SPRTF("pwd [%s], salt [%s], result [%s] - Access %s\n", pass, pass_hash, result, (ok ? "granted" : "denied")); 
    // puts(ok ? "Access granted." : "Access denied.");
    if (!ok) {
        char *new_hash = strdup(result);
        memset(cb,0,sizeof(data));
        char *nres = MCRYPT(pass, new_hash, cb);
        ok = strcmp (nres, new_hash) == 0;
        SPRTF("pwd [%s], salt [%s], result [%s] - Access %s\n", pass, new_hash, nres, (ok ? "granted" : "denied")); 
    }
    return ok ? 0 : 1;
}

char *cmd_pass = 0;

void parse_command(int argc, char **argv)
{
    int i, i2, c;
    char *arg, *sarg;
    char *cp = _s_buffer;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c) {
            case 'h':
            case '?':
                give_help(argv[0]);
                break;
            case 'p':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    cmd_pass = strdup(sarg);
                    //test2(sarg);
                    c = pass_matches( int_pwd, sarg );
                    SPRTF("pwd [%s] salt [%s] results %s\n", sarg, int_pwd, (c ? "ok" : "failed") );
                    // can not get this right????????????????
                    // pass_matches ( u->password, password ) matches config password, with the new input
                    // but what is all this about a PREFIX???
                    *cp = 0;
                    //strcpy(cp,MD5_PREFIX);
                    //strcat(cp,sarg);
                    strcat(cp,md5_pwd);
                    //c = pass_matches( cp, md5_pwd );
                    //c = pass_matches( md5_pwd, cp );
                    c = pass_matches2( cp, sarg );
                    SPRTF("pwd [%s] salt [%s] result2 %s\n", sarg, cp, (c ? "ok" : "failed") );

#if 0 // BAH this all FAILS
                    // try a simple crypt()
                    strcpy(cp,md5_pwd);
                    char *pass = cp;
                	if ( (!strncasecmp ( pass, DES_PREFIX, sizeof ( DES_PREFIX )-1 ) ) ) {
                        pass += sizeof ( DES_PREFIX )-1;
	                }
                    char *cp2 = pass + strlen(pass) + 1;
                    strcpy(cp2,sarg);
                    char *test = crypt( cp2, pass );
                    SPRTF("crypt of [%s], salt [%s], yielded [%s]\n", cp2, pass, test);

                    strcpy(cp,md5_pwd);
                    c = pass_matches2( cp, sarg );
                    SPRTF("pwd [%s] result2 %s\n", sarg, (c ? "ok" : "failed") );
#endif

                } else {
                    fprintf(stderr,"ERROR: %s must be followed by a password\n", arg);
                    goto Bad_Arg;
                }
                break;
            default:
Bad_Arg:
                fprintf(stderr,"ERROR: Unknown argument [%s]\n", arg);
                exit(1);
                break;
            }
        } else {
            out_file = strdup(arg);
        }
    }
}

#ifdef WIN32
#define UL_WIN32
#endif
/* Init/Exit functions */
static void _netExit ( void )
{
#if defined(UL_CYGWIN) || !defined (UL_WIN32)
#else
	/* Clean up windows networking */
	if ( WSACleanup() == SOCKET_ERROR ) {
		if ( WSAGetLastError() == WSAEINPROGRESS ) {
			WSACancelBlockingCall();
			WSACleanup();
		}
	}
#endif
}


static int _netInit ()
{
  // assert ( sizeof(sockaddr_in) == sizeof(netAddress) ) ;
#if defined(UL_CYGWIN) || !defined (UL_WIN32)
  /* nothing to do here ... */
#else
	/* Start up the windows networking */
	WORD version_wanted = MAKEWORD(2,2);
	WSADATA wsaData;

	if ( WSAStartup(version_wanted, &wsaData) != 0 ) {
		printf("Couldn't initialize Winsock 1.1\n");
		return(-1);
	}
#endif

    atexit( _netExit ) ;
	return(0);
}


int main(int argc, char **argv) 
{
	int iret;
    test3((char *)"GNU libc manual");
    parse_command(argc,argv);
	iret = write_image((char *)out_file);
    if (cmd_pass) {
        test3(cmd_pass);
        test2(cmd_pass);
    }
	return iret;
}

// eof - ex.cxx
