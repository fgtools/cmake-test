
/**********************************************************************
  my_crypt.c
 **********************************************************************

  my_crypt - Clone of the UNIX crypt (1) utility.

  The latest version of this program should be available at:
  http://saa.dyndns.org/stewart
  from : http://saa.dyndns.org/stewart/Software/my_crypt.c.html
  
  Copyright ©2001, 2003, 2009, Stewart Adcock <stewart--AT--linux-domain.com>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
 
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
  OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
  WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.

 **********************************************************************

  synopsis:     Dodgy clone of the dodgy UNIX crypt utility.

  compilation:  try typing something like this with the GNU compiler:
                gcc my_crypt.c -o crypt -lcrypt -Wall -O2

  usage:        crypt [-s] passkey < file > encrypted_file
                crypt [-s] passkey < encrypted_file > file

                Warning: This _IS_ _NOT_ designed to be
                cryptographically secure.  It was solely written as
                a means to decrypt files which were encrypted using
                the legacy UNIX crypt utility.

  Last Updated: 01 June 2009 SAA  Re-released under the terms of the 2-clause BSD license.
                28 May 2003  SAA  Fixed a bug - thanks to Dr A J Bartlett for pointing this out.
                11 June 2001 SAA  Clone of UNIX crypt utility.

 **********************************************************************/

#define _XOPEN_SOURCE   /* For crypt() */
#define _ISO99_SOURCE   /* For int32_t */

#include <stdio.h>
#include <stdint.h>     /* Comment out this line for MIPSpro compiler on IRIX, and possibly others. */
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
static struct termios told, tnew;

/* Initialize new terminal i/o settings */
void initTermios(int echo) 
{
  tcgetattr(0, &told); /* grab old terminal i/o settings */
  tnew = told; /* make new settings same as old settings */
  tnew.c_lflag &= ~ICANON; /* disable buffered i/o */
  tnew.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
  tcsetattr(0, TCSANOW, &tnew); /* use these new terminal i/o settings now */
}

/* Restore old terminal i/o settings */
void resetTermios(void) 
{
  tcsetattr(0, TCSANOW, &told);
}

/* Read 1 character - echo defines echo mode */
char getch_(int echo) 
{
  char ch;
  initTermios(echo);
  ch = getchar();
  resetTermios();
  return ch;
}

/* Read 1 character without echo */
char getch(void) 
{
  return getch_(0);
}

/* Read 1 character with echo */
char getche(void) 
{
  return getch_(1);
}

#define NUM_ROTORS      256
#define MASK            0377

static char    t1[NUM_ROTORS];
static char    t2[NUM_ROTORS];
static char    t3[NUM_ROTORS];
static char    deck[NUM_ROTORS];

static char *init(char *key)
{
  int           ic, i, j, temp;
  unsigned int  random;
  int32_t       seed=123;
  char          *password;

  password = (char *)crypt(key, key);

  for (i=0; i<13; i++) seed = seed*password[i] + i;

  for(i=0;i<NUM_ROTORS;i++)
  {
    t1[i] = i;
    deck[i] = i;
    t2[i] = 0;
    t3[i] = 0;
  }

  for(i=0;i<NUM_ROTORS;i++)
  {
    seed = 5*seed + password[i%13];
    random = seed % 65521;
    j = NUM_ROTORS - i;
    ic = (random&MASK) % j;
    j--;
    random >>= 8;
    temp = t1[ic];
    t1[ic] = t1[j];
    t1[j] = temp;
    if (t3[j]==0)
    {
      ic = (random&MASK) % j;
      while (t3[ic]!=0) ic = (ic+1) % j;
      t3[ic] = j;
      t3[j] = ic;
    }
  }

  for(i=0;i<NUM_ROTORS;i++) t2[t1[i]&MASK] = i;

  return password;
}


static void shuffle(char *deck, char *password)
{
  int                   ic, i, j, temp;
  static int32_t        seed = 123;

  for(i=0;i<NUM_ROTORS;i++)
  {
    seed = 5*seed + password[i%13];
    j = NUM_ROTORS - i;
    ic = ((seed%65521)&MASK)%j;
    j--;
    temp = deck[j];
    deck[j] = deck[ic];
    deck[ic] = temp;
  }

  return;
}

static void print_hex( char *buf, int len )
{
    static char ascii[16+1];
    int i, cnt, c;
    if (len == 0)
        return;
    cnt = 0;
    for (i = 0; i < len; i++) {
        c = buf[i];
        printf("%02X ", (c & 0xff));
        if (c < ' ')
            c = '.';
        else if (c >= 0x7f)
            c = '.';
        ascii[cnt++] = (char)c;
        if (cnt == 16) {
            ascii[cnt] = 0;
            printf("%s\n", ascii);
            cnt = 0;
        }
    }
    if (cnt) {
        ascii[cnt] = 0;
        printf("%s\n", ascii);
    }
}

#define MX_CHARS 1024
static char results[MX_CHARS+2];
static char entered[MX_CHARS+2];

int main(int argc, char **argv)
{
  int   i, res, cnt = 0;
  int   n1=0, n2=0, nr1=0, nr2=0;
  int   soption = 0;
  int   verbose = 0;
  char  *password;
  
  if (argc < 2) 
  {
    printf("Usage: %s [-s] key\n", argv[0]);
    printf("Given a key, then enter a password, encypt and show output.\n");
    printf("The -s switch adds a 'shuffle' into the mix.\n");
    printf("The -v switch sets verbosity.\n");
    return 1;
  }
  
  /* Check for the '-s' switch, the only supported option. */
  if (argc > 2)
  {
    if (argv[1][0] == '-')
    {
        if (argv[1][1] == 's')
        {
            soption = 1;
            printf("Setting shuffle switch...\n");
        } else
        if (argv[1][1] == 'v')
        {
            verbose = 1;
            printf("Setting verbose switch...\n");
        } else {
            goto Bad_Arg;
        }
        argc--;
        argv++;
    }
    else
    {
Bad_Arg:    
        printf("Only switches allowed are -s or -v! Not [%s]\n", argv[1]);
        return 1;
    }
  }

  printf("Doing init with key [%s]\n", argv[1]);
  password = init(argv[1]);
  printf("Results [%s]\n", password);

    printf("Input password:\n");
    cnt = 0;
    //while((i = getchar()) > ' ')
    while((i = getch()) > ' ')
    {
      entered[cnt] = (char)i;
      
      nr1 = deck[n1]  & MASK;
      nr2 = deck[nr1] & MASK;
      
      res = (t2[(t3[(t1[(i+nr1)&MASK]+nr2)&MASK]-nr2)&MASK]-nr1);

      results[cnt++] = (char)res;
      if (verbose) {
          printf("%d %c %02X\n", cnt, (char)i, (res & 0xff));
      } else {
        putchar(i);
      }
      n1++;
      if (n1==NUM_ROTORS)
      {
        n1 = 0;
        n2++;
        
        if(n2==NUM_ROTORS) n2 = 0;

        if (soption) {
            shuffle(deck, password);
        }
      }
      if (cnt == MX_CHARS)
        break;
    }
    printf("\n");
    if (cnt) {
      results[cnt] = 0;
      printf("%d results: ", cnt);
      print_hex(results,cnt);
    }
    return 0;
}

/* ORIGINAL CODE */
#if 0 // *******************

**********************************************************************
  my_crypt.c
 **********************************************************************

  my_crypt - Clone of the UNIX crypt (1) utility.

  The latest version of this program should be available at:
  http://saa.dyndns.org/stewart

  Copyright ©2001, 2003, 2009, Stewart Adcock <stewart--AT--linux-domain.com>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
 
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
  OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
  WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.

 **********************************************************************

  synopsis:     Dodgy clone of the dodgy UNIX crypt utility.

  compilation:  try typing something like this with the GNU compiler:
                gcc my_crypt.c -o crypt -lcrypt -Wall -O2

  usage:        crypt [-s] passkey < file > encrypted_file
                crypt [-s] passkey < encrypted_file > file

                Warning: This _IS_ _NOT_ designed to be
                cryptographically secure.  It was solely written as
                a means to decrypt files which were encrypted using
                the legacy UNIX crypt utility.

  Last Updated: 01 June 2009 SAA  Re-released under the terms of the 2-clause BSD license.
                28 May 2003  SAA  Fixed a bug - thanks to Dr A J Bartlett for pointing this out.
                11 June 2001 SAA  Clone of UNIX crypt utility.

 **********************************************************************/

#define _XOPEN_SOURCE   /* For crypt() */
#define _ISO99_SOURCE   /* For int32_t */

#include <stdio.h>
#include <stdint.h>     /* Comment out this line for MIPSpro compiler on IRIX, and possibly others. */
#include <stdlib.h>
#include <unistd.h>

#define NUM_ROTORS      256
#define MASK            0377

char    t1[NUM_ROTORS];
char    t2[NUM_ROTORS];
char    t3[NUM_ROTORS];
char    deck[NUM_ROTORS];

char *init(char *key)
  {
  int           ic, i, j, temp;
  unsigned int  random;
  int32_t       seed=123;
  char          *password;

  password = (char *)crypt(key, key);

  for (i=0; i<13; i++) seed = seed*password[i] + i;

  for(i=0;i<NUM_ROTORS;i++)
    {
    t1[i] = i;
    deck[i] = i;
    t2[i] = 0;
    t3[i] = 0;
    }

  for(i=0;i<NUM_ROTORS;i++)
    {
    seed = 5*seed + password[i%13];
    random = seed % 65521;
    j = NUM_ROTORS - i;
    ic = (random&MASK) % j;
    j--;
    random >>= 8;
    temp = t1[ic];
    t1[ic] = t1[j];
    t1[j] = temp;
    if (t3[j]==0)
      {
      ic = (random&MASK) % j;
      while (t3[ic]!=0) ic = (ic+1) % j;
      t3[ic] = j;
      t3[j] = ic;
      }
    }

  for(i=0;i<NUM_ROTORS;i++) t2[t1[i]&MASK] = i;

  return password;
  }


void shuffle(char *deck, char *password)
  {
  int                   ic, i, j, temp;
  static int32_t        seed = 123;

  for(i=0;i<NUM_ROTORS;i++)
    {
    seed = 5*seed + password[i%13];
    j = NUM_ROTORS - i;
    ic = ((seed%65521)&MASK)%j;
    j--;
    temp = deck[j];
    deck[j] = deck[ic];
    deck[ic] = temp;
    }

  return;
  }


int main(int argc, char **argv)
  {
  int   i;
  int   n1=0, n2=0, nr1=0, nr2=0;
  int   soption = 0;
  char  *password;

/* Check for the '-s' switch, the only supported option. */
  if (argc > 1 && argv[1][0] == '-' && argv[1][1] == 's')
    {
    argc--;
    argv++;
    soption = 1;
    }

  password = init(argv[1]);

  if (soption)
    {
    while((i=getchar())>=0)
      {
      nr1 = deck[n1]&MASK;
      nr2 = deck[nr1]&MASK;

      i = t2[(t3[(t1[(i+nr1)&MASK]+nr2)&MASK]-nr2)&MASK]-nr1;
      putchar(i);
      n1++;
      if (n1==NUM_ROTORS)
        {
        n1 = 0;
        n2++;
        if(n2==NUM_ROTORS) n2 = 0;

        shuffle(deck, password);
        }
      }
    }
  else
    {
    while((i=getchar())>=0)
      {
      nr1 = n1;

      i = t2[(t3[(t1[(i+nr1)&MASK]+nr2)&MASK]-nr2)&MASK]-nr1;
      putchar(i);
      n1++;
      if (n1==NUM_ROTORS)
        {
        n1 = 0;
        n2++;
        if(n2==NUM_ROTORS) n2 = 0;

        nr2 = n2;
        }
      }
    }

  exit(0);
  }

#endif // 0 ****************
  
/* eof */

