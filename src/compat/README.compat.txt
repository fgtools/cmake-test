README.compat.txt - 20140206

Always searching for compatible replacement functions when an OS 
fails to provide certain functions, like getopt say in windows...

from : http://sources.debian.net/src/dpkg/1.17.6/lib/compat

This looks like a neat set to build a compat.lib, depending on 
what is available in the system.

Makefile.am - original automake file - used to help build CMakeLists.txt
alphasort.c - compares a struct dirent.name with strcmp()
asprintf.c  - uses vasprintf(dest, fmt, args)
compat.h    - header with switches like HAVE_ASPRINTF, HAVE_STRNDUP, etc...
empty.c     - just defines a public dummy symbol so the library is never empty
getopt.c    - implementation of getopt, and has a #ifdef TEST main() to test the function
getopt.h    - declares getopt_long(...) - needs __STDC__ defined as 1
getopt1.c   - same as getopt.c except no getopt(), only getopt_long(...), with TEST
gettext.h   - #if ENABLE_NLS - gettext macros/functions...
md5.c       - implements the MD5 message-digest algorithm.
md5.h       - header for above MD5Init, MD5Update, etc...
obstack.c   - subroutines used implicitly by object stack macros - looks UNIX only?
obstack.h   - header for above, but uses __extension__ and __attribute__ in .c file
scandir.c   - needs <dirent.h>, which is not here?
snprintf.c  - uses vsnprintf(str, n, fmt, args);
strerror.c  - needs gettext(str) and extern const char *const sys_errlist[]; extern const int sys_nerr;
strndup.c   - uses strnlen, and malloc to allocate, tests allocation, and memcpy zero terminated
strnlen.c   - uses memchr (string, '\0', maxlen);
strnlen.h   - header for the above
strsignal.c - returns signal name for 1 - 22 signals - unix only
unsetenv.c  - uses malloc and putenv(q)
vasprintf.c - uses vsnprintf(NULL,...) to get length, then malloc
vsnprintf.c - auggests unix only since unconditionally include <unistd.h> NOT in windows

# eof

