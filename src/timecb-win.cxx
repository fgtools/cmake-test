
// timecb-win.cxx

#include <windows.h>
#include <stdio.h>
#include "ufc/crypt.h"

#define USE_PEEK_MESSAGE

static bool bStillBusy = false;
static double to_secs = 3.0;
static int msg_count = 0;
static __int64 cyc_count = 0;

static void CALLBACK TimerProc(HWND, UINT, UINT, DWORD);

///////////////////////////////////////////////////////////////////////////////
#define USE_PERF_COUNTER
///////////////////////////////////////////////////////////////////////////////
#if (defined(WIN32) && defined(USE_PERF_COUNTER))
// QueryPerformanceFrequency( &frequency ) ;
// QueryPerformanceCounter(&timer->start) ;
double get_seconds()
{
    static double dfreq;
    static int done_freq = 0;
    static int got_perf_cnt = 0;
    double d;
    if (!done_freq) {
        LARGE_INTEGER frequency;
        if (QueryPerformanceFrequency( &frequency )) {
            got_perf_cnt = 1;
            dfreq = (double)frequency.QuadPart;
        }
        done_freq = 1;
    }
    if (got_perf_cnt) {
        LARGE_INTEGER counter;
        QueryPerformanceCounter (&counter);
        d = (double)counter.QuadPart / dfreq;
    }  else {
        DWORD dwd = GetTickCount(); // milliseconds that have elapsed since the system was started
        d = (double)dwd / 1000.0;
    }
    return d;
}

#else // !WIN32
double get_seconds()
{
    struct timeval tv;
    gettimeofday(&tv,0);
    double t1 = (double)(tv.tv_sec+((double)tv.tv_usec/1000000.0));
    return t1;
}
#endif // WIN32 y/n
///////////////////////////////////////////////////////////////////////////////
static int do_crypt = 1; // some heavy crypt() (bit splitting) work

void do_something()
{
    // do some work
    if (do_crypt) {
        const char *s = "fredred";
        char *cp = ufc_crypt( s, "eek" );
    }
    cyc_count++;
}

char *get_nn(char *num) // nice number nicenum add commas
{
    static char _s_nn[264];
    char *cp = _s_nn;
    size_t len = strlen(num);
    size_t ii, jj, kk, out;
    if (len <= 3) {
        strcpy(cp,num);
    } else {
        ///if (len > 3) {
        size_t mod = len % 3;
        size_t max = (int)( len / 3 );
        ii = 0;
        for (; ii < mod; ii++) {
            cp[ii] = num[ii]; // copy in upper numbers, if any
        }
        cp[ii] = 0;
        out = ii;
		for (jj = 0; jj < max; jj++ ) {
            if ( !((mod == 0) && (jj == 0)) )
                cp[out++] = ',';    // add in comma
            for (kk = 0; kk < 3; kk++) {
                // then 3 numbers
                cp[out++] = num[ii + (jj*3) + kk];
            }
		}
		cp[out] = 0;
	}
	return cp;
}

char *get_nn64( __int64 val )
{
    static char _s_nn64[264];
    char *cp = _s_nn64;
    sprintf(cp,"%I64d",val);
    cp = get_nn(cp);
    return cp;
}
char *get_nn32( int val )
{
    static char _s_nn32[264];
    char *cp = _s_nn32;
    sprintf(cp,"%d",val);
    cp = get_nn(cp);
    return cp;
}

int main(int argc, char **argv)
{
    UINT id;
    MSG msg;
    if (argc > 1) {
        to_secs = atof(argv[1]);
        printf("Setting timeout to %f seconds.\n", to_secs);
    } else {
        printf("Using default timeout of %f seconds.\n", to_secs);
    }
    bStillBusy  = true;

    // The time-out value, in milliseconds
    id = SetTimer(NULL, 0, (int)(to_secs * 1000.0), (TIMERPROC) TimerProc);
    if (id) {
        printf("Got timer ID = %d\n", id );
    } else {
        printf("Failed to create a timer!\n");
        return 1;
    }
    double start_time = get_seconds();
    while(bStillBusy) 
    {
#ifdef USE_PEEK_MESSAGE
        if (PeekMessage(&msg,NULL,0,0,PM_REMOVE)) {
            DispatchMessage(&msg);
            msg_count++;
        } else {
            do_something();
        }
#else
        GetMessage(&msg, NULL, 0, 0);
        DispatchMessage(&msg);
        msg_count++;
#endif

    }
    double elapsed = get_seconds() - start_time;
    KillTimer(NULL, id);
    printf("Processed %d messages...\n", msg_count);
#ifdef USE_PEEK_MESSAGE
    if (elapsed > 0.0) {
        printf("Ran %s %s, ", get_nn64(cyc_count),
            (do_crypt ? "crypt()" : "cycles"));
        printf("in %f secs = %s cps\n", elapsed, get_nn32((int)((double)cyc_count / elapsed)));
    }
#endif
    return 0;
}

void CALLBACK TimerProc(HWND hwnd, UINT uMsg, UINT idEvent, DWORD dwTime) 
{
   printf("Timer func got called\n");
   bStillBusy = false;
}

/* eof */


