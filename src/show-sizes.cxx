/*\
 * example.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
 * Just a place to add any 'code' tests...
 *
\*/

#include <stdio.h>
#include <stdint.h>
#include <limits.h>
#include <float.h>
#include <iostream>
#include <iomanip> // needed to use manipulators with parameters (precision, width)
#include <string>
#include <sstream>

#include "sprtf.hxx"
#include "utils.hxx"

static const char *module = "example";
static const char *def_log_file = "tempexam.txt";
static void show_sizes()
{
    SPRTF("Size of char %d, short %d, int %d, long %d, long long %d, uint64_t %d\n", 
        sizeof(char), sizeof(short), sizeof(int), sizeof(long), sizeof(long long),
        sizeof(uint64_t));
    char c = CHAR_MAX;
    short s = SHRT_MAX;
    int i = INT_MAX;
    long long l = LLONG_MAX;
    SPRTF("Max.Val char %d, short %d, int %d, long long %lld\n",
        c, s, i, l);
    SPRTF("Size of float %d, double %d, long double %d, void * %d\n",
        sizeof(float), sizeof(double), sizeof(long double),
        sizeof(void *));
    float f = FLT_MAX;
    double d = DBL_MAX;
    char *cpf = GetNxtBuf();
    char *cpd = double_to_stg(d);
    sprintf(cpf,"%f", f);
    trim_float_buf(cpf);
    SPRTF("Max.Val float %s\n"
          "       double %s\n",
        cpf, cpd);
    std::ostringstream msg;
    msg << "float " << f << ", double " << d;
    SPRTF("or in scientific notation: %s\n", msg.str().c_str());
    i = INT_MIN;
}

// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    set_log_file((char *)def_log_file,false);
    SPRTF("%s: Running...\n", module);
    if (argc > 1) {
        std::ostringstream msg;
        int i = argc - 1;
        msg << "Got " << i << " commands..." << std::endl;
        for (i = 1; i < argc; i++) {
            msg << i << ": '" << argv[i] << "'" << std::endl;
        }
        SPRTF("%s", msg.str().c_str());
    }
    show_sizes();
    return iret;
}


// eof = example.cxx
