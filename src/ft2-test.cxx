/*\
 * ft2-test.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <stdio.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_WINFONTS_H
/* ==========================================
   UGH! These are 'internal' and NOT installed!
   That is can ONLY use these IFF building in the library SOURCE,
   so include/internal/<headers>... can be found.
#include FT_INTERNAL_DEBUG_H
#include FT_INTERNAL_STREAM_H
#include FT_INTERNAL_OBJECTS_H
  =========================================== */

static const char *module = "ft2-test";
static FT_Library       library;

// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    FT_Error    error = FT_Err_Ok;
    FT_Int      amajor,aminor,apatch;
    printf("%s: Found and linked with FreeType library.\n",module);
    error = FT_Init_FreeType( &library );
    if ( error ) {
        printf("%s: FT_Init_FreeType( &library ); FAILED %d\n", module, error );
        return error;
    }
    FT_Library_Version( library, &amajor, &aminor, &apatch );
    printf("%s: FreeType Library Version %d.%d.%d\n", module, amajor, aminor, apatch );

    FT_Done_FreeType( library );

    return iret;
}


// eof = ft2-test.cxx
