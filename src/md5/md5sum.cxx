// md5sum.cxx

#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "md5.hxx"

int main( int argc, char **argv )
{
   FILE *fp;

   if (argc < 2) {
       printf("usage: md5sum input-file\n");
       return 1;
   }
   fp = fopen(argv[1],"rb");
   if (!fp) {
       printf("Error: Unable to open file %s\n", argv[1]);
       return 1;
   }
   if (fseek(fp,0,SEEK_END)) {
       printf("Error: seek(END) failed\n");
       fclose(fp);
       return 1;
   }
   size_t sz = ftell(fp);
   if (fseek(fp,0,SEEK_SET)) {
       printf("Error: seek(BGN) failed\n");
       fclose(fp);
       return 1;
   }
   if (sz <= 0) {
       printf("Error: ftell(fp) failed, or size is zero\n");
       fclose(fp);
       return 1;
   }
   char *cp = (char *)malloc(sz + 2);
   if (!cp) {
       printf("Error: allocation of %ld bytes failed!\n", (sz+2));
       fclose(fp);
       return 1;
   }
   size_t red = fread(cp,1,sz,fp);
   fclose(fp);
   if (red != sz) {
       printf("Error: read of file failed! req %ld, recvd %ld\n", sz, red );
       free(cp);
       return 1;
   }
   cp[sz] = 0;
   std::string s(cp);
   free(cp);
   MD5 *pmd5 = new MD5(s);
   s = pmd5->hexdigest();
   delete pmd5;
   printf("%s %s\n", s.c_str(), argv[1] );

   return 0;
}

/* eof */

