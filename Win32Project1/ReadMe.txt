========================================================================
    WIN32 APPLICATION : Win32Project1 Project Overview
========================================================================

Was originally created by the AppWizard, but then quite a number of modifications were made, the main one being the removal of StdAfx.h and StdAfx.cpp, and of course its build was added to the root CMakeLists.txt

Win32Project1.cpp, Win32Project1.h
    This is the main application source files.

/////////////////////////////////////////////////////////////////////////////
AppWizard has created the following resources:

Win32Project1.rc
    This is a listing of all of the Microsoft Windows resources that the
    program uses.  It includes the icons, bitmaps, and cursors that are stored
    in the RES subdirectory.  This file can be directly edited in Microsoft
    Visual C++.

Resource.h
    This is the standard header file, which defines new resource IDs.
    Microsoft Visual C++ reads and updates this file.

Win32Project1.ico
    This is an icon file, which is used as the application's icon (32x32).
    This icon is included by the main resource file Win32Project1.rc.

small.ico
    This is an icon file, which contains a smaller version (16x16)
    of the application's icon. This icon is included by the main resource
    file Win32Project1.rc.

/////////////////////////////////////////////////////////////////////////////
Other notes:

Of course it is a Windows app so is only built `if (WIN32)`...

/////////////////////////////////////////////////////////////////////////////
