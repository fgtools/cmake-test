README.cmake-find.txt

Some research on where to INSTALL a 'private' FindMyLIB.cmake module.

from : http://stackoverflow.com/questions/10765885/how-to-install-your-custom-cmake-find-module

This STRONGLY suggests that they NOT be installed in the cmake 'modules' folder,
since this would be a 'custom' find module, and probably should NOT be added to 
there. That is ${CMAKE_ROOT}/Modules/

In my Windows 7 machine, with cmake 3.2 that path is -
C:\Program Files (x86)\CMake\share\cmake-3.2\Modules
In my Ubuntu 14.04 machine, with cmake 2.8 that path is -
/usr/share/cmake-2.8/Modules

But it then suggests some places where the cmake FIND_PACKAGE might look, and 
these seem a suitable install location. It does seem to depend on the OS!

1.  <prefix>/                                               (Windows)
2.  <prefix>/(cmake|CMake)/                                 (Windows)
3.  <prefix>/<name>*/                                       (Windows)
4.  <prefix>/<name>*/(cmake|CMake)/                         (Windows)
5.  <prefix>/(lib/<arch>|lib|share)/cmake/<name>*/          (Unix)
6.  <prefix>/(lib/<arch>|lib|share)/<name>*/                (Unix)
7.  <prefix>/(lib/<arch>|lib|share)/<name>*/(cmake|CMake)/  (Unix)
8.  <prefix>/<name>.framework/Resources/                    (Apple)
9.  <prefix>/<name>.framework/Resources/CMake/              (Apple)
10. <prefix>/<name>.framework/Versions/*/Resources/         (Apple)
11. <prefix>/<name>.framework/Versions/*/Resources/CMake/   (Apple)
12. <prefix>/<name>.app/Contents/Resources/                 (Apple)
13. <prefix>/<name>.app/Contents/Resources/CMake/           (Apple)

Having already built some packages that do install into the CMAKE_INSTALL_PREFIX 
path. Some examples -

Checking: prefix=F:\Projects\software
HDF5: Uses <prefix>\cmake\hdf5\FindHDF5.cmake, which does not seem to 
exactly fit the above.

Checking: prefix=F:\Projects\software.x64
GLM: Uses <prefix>\lib\cmake\FindGLM.cmake, which again does not seem to 
exactly fit the above.

Hmmm, not many examples to draw from!!!

But in reading further it seems cmake also searches for a <package>config.cmake,
and perhaps even a <package>config-version.cmake, and a few of these have 
also been installed -

Checking: prefix=F:\Projects\software
HDF5: Uses <prefix>\cmake\hdf5\hdf5-config.cmake, and hdf5-conifg.version.cmake
OpenCV: Uses <prefix>\OpenCVConfig.cmake, and OpenCVConfig-version.cmake
SZIP: Uses <prefix>\cmake\SZIP\szip-config.cmake, and szip-config-version.cmake
ZLIB: Uses <prefix>\cmake\ZLIB\zlib-config.cmake, and zlib-config-version.cmake
glfw: Uses <prefix>\lib\cmake\glfw\glfw3Config.cmake, and glfw3ConfigVersion.cmake
netcdf: Uses <prefix>\share\cmake\netcdf-config.cmake, and netcdf-config-vesion.cmake

Are these 'generated', or static -
Well HDF5 uses
configure_file (
    ${HDF_RESOURCES_DIR}/hdf5-config.cmake.build.in 
    ${HDF5_BINARY_DIR}/${HDF5_PACKAGE}${HDF_PACKAGE_EXT}-config.cmake @ONLY)
    
And it uses an *.in file for each of the others, including FindHDF5.cmake.in

And while it employs the same header find as other 'find' modules, like -
FIND_PATH(HDF5_INCLUDE_DIRS "H5public.h" 
HINTS ${_HDF5_HINTS} PATHS ${_HDF5_PATHS} PATH_SUFFIXES
cmake/@HDF5_PACKAGE@ lib/cmake/@HDF5_PACKAGE@ share/cmake/@HDF5_PACKAGE@)

to set the libraries is uses -
FIND_PATH (HDF5_ROOT_DIR "@HDF5_PACKAGE@@HDF_PACKAGE_EXT@-config.cmake"
    HINTS ${_HDF5_HINTS} PATHS ${_HDF5_PATHS} PATH_SUFFIXES
    cmake/@HDF5_PACKAGE@ lib/cmake/@HDF5_PACKAGE@ share/cmake/@HDF5_PACKAGE@)
and rather than doing any 'library' search, uses -
  include (${HDF5_ROOT_DIR}/@HDF5_PACKAGE@@HDF_PACKAGE_EXT@-config.cmake)
for getting the library list.

Lot to take in here... Needs a lot of setup to make all this work in HDF5,
but hopefully it is worth it...

; eof
