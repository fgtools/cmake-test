# CMake test Projects

README.md - 20150626 - 20140717 - 20140206 - 20130718

Just mainly 'experiments' and 'testing' CMakelists.txt things, 
expecially 'finding' things...

#### 20141210:

When trying to compile xptools (x-plane) in linux got an error missing <ft2build.h>
thus here added a find_package(Freetype), and found I needed to add -I/usr/include/freetype2
to the build process.

But the find_package FAILED in Windows! Had to copy the cmake release FindFreetype.cmake to 
the local CMakeModules directory, and make some small modifications.

Then added a ft2-test.cxx module to make sure all could be found.

#### 20140722:

Because tests using include (CheckSymbolExists), like
check_symbol_exists( uint64_t stdint.h HAVE_UINT64_T )
seem to mostly FAIL, see below, added a CHECK_SYMBOL macros that takes
the same values as check_symbol_exists(), but uses a compile of 
some code for the test.

This seems to 'find' the particular symbol.

The check_symbol_exists() seems to also use a compile of some code, but my reading of that 
code determines it will always FAIL!?!?

The code used (as shown in the CMakeFiles/CmakeError.log) is -
```
/* */
#include <stdint.h>

int main(int argc, char** argv)
{
  (void)argv;
#ifndef uint64_t
  return ((int*)(&uint64_t))[argc];
#else
  (void)argc;
  return 0;
#endif
}
```

This seems wrong on two counts -

 1. Even though uint64_t is in stdint.h, it is a 'typedef', so the first
#ifndef uint64_t will be true, so
 2. The compiler will try to compile return ((int*)(&uint64_t))[argc]; which 
is the wrong use of a typedefined uint64_t, so no compile.

Tried hard to search for this case in the cmake documents, and bug reports 
but nothing specific found.

I am now 'happy' my own CHECK_SYMBOL(a b c) macro seems to do the job well ;=))

After looking at cmake-2.8/Modules/CheckSymbolExists.cmake, maybe should ADD 
the lines
```
if (FOUND)
      set(${VARIABLE} 1 CACHE INTERNAL "Have symbol ${SYMBOL}")
else ()
      set(${VARIABLE} "" CACHE INTERNAL "Have symbol ${SYMBOL}")
endif ()
```

This MAY avoid repeating the test when running cmake again... Yes, this seems 
to work fine.

#### 20140717:

Added some tests using include (CheckSymbolExists), like
check_symbol_exists( uint64_t stdint.h HAVE_UINT64_T )
HOWEVER, for some reason this 'test' FAILS???

So added another compile test -
```
check_c_source_compiles("
    #include <stdint.h>
    int main(int argc, char **argv) {
      uint64_t val = 0;
      return 0;}" CAN_COMP_UINT64_T)
```

and it successfuly COMPILES!!!

Which suggests the `check_symbol_exists(type header variable)`
can FAIL ;=(( Maybe this is because it is defined using a 'typedef' statement, rather 
than any simple `#define ...` statement. NOT SURE???

And ANOTHER test whihc find the SIZEOF_UINT64_T also works well -

```
set(CMAKE_EXTRA_INCLUDE_FILES stdint.h)
check_type_size(uint64_t SIZEOF_UINT64_T)
set(CMAKE_EXTRA_INCLUDE_FILES)
if (SIZEOF_UINT64_T)
    message(STATUS "*** The SIZEOF 'uin64_t' is ${SIZEOF_UINT64_T}")
else ()
    message(STATUS "*** The SIZEOF 'uin64_t' NOT FOUND")
endif ()
```

Output:  
-- Check size of uint64_t  
-- Check size of uint64_t - done  
-- *** The SIZEOF 'uin64_t' is 8  

So again this implies one should be VERY WARY of the results of 'check_symbol_exists()'!!!

#### 20140206:

Added a compat.lib source downloaded from http://sources.debian.net/src/dpkg/1.17.6/lib/compat
See src/compat/README.compat.txt for more details.

Went from just 'experiments' and 'testing' CMakelists.txt things,
to also testing and experimenting with the UNIX crypt(3) function 
especially in window using sources downloaded from various places.

But this proved 'difficult' to get it right. The two sources I 
downloaded lead to different results! BAH! 

Then tried to 'verify' the results using the crypt library in unix.
Some success but still not completely happy ;=((

Found another source at - http://ftp.nluug.nl/crypto/gnu/  
DOWN: glibc-crypt-2.0.6.tar.gz  28-Dec-1997 00:00 28K  
With considerable modifications got it to compile in Windows 7
using MSVC10.

It includes some 'test' programs, one call cert.c, which reads 
a cert.input, with 171 tests in 3 columns - key plain answer.
It sets up the key using 'setkey' in the library, which calls
__setkey_r(key, &_ufc_foobar) which is stuct crypt_data.

Then copies the plain into a cipher block, and does 
encryt(cipher,0), and compares the result with the answer, 
bumping 'fail' if not exact. Then does the reverse encrypt(cipher,1)
and compares the result to plain, bumping fail if not the same.

It passed all 171 tests ;=)) Starting to feel confident about 
this code.

Geoff.

; eof
